<?php

namespace Rumedo\Banner;

/**
 * Class Course
 *
 * Хранит в себе информацию о курсе для баннера
 *
 * @package Rumedo\Banner
 */
class Course
{
	/** @var string Дата начала курса в формате "05.07.17" */
	private $dateStart;

	/** @var string Время окончания курса в формате "14:00" */
	private $timeStart;

	/** @var string Дата окончания курса в формате "05.07.17" */
	private $dateEnd;

	/** @var string Время окончания курса в формате "14:00" */
	private $timeEnd;

	/** @var string Заголовок курса */
	private $title;

	/** @var string Url курса */
	private $url;

	/** @var boolean Пустой ли объект */
	private $empty;


	/**
	 * Course constructor.
	 *
	 * @param $data array
	 */
	public function __construct($data) {
		if($data) {
			$this->dateStart = $data['date_start'];
			$this->timeStart = $data['time_start'];
			$this->dateEnd = $data['date_end'];
			$this->timeEnd = $data['time_end'];
			$this->title = $data['title'];
			$this->url = $data['url'];

			$this->empty = false;
		} else {
			$this->empty = true;
		}
	}

	/**
	 * Пустой ли объект курса
	 *
	 * @return bool
	 */
	public function isEmpty()
	{
		if($this->empty) {
			return true;
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getDateStart() {
		return $this->dateStart;
	}

	/**
	 * @return string
	 */
	public function getTimeStart() {
		return $this->timeStart;
	}

	/**
	 * @return string
	 */
	public function getDateEnd() {
		return $this->dateEnd;
	}

	/**
	 * @return string
	 */
	public function getTimeEnd() {
		return $this->timeEnd;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Сериализует курс в массив
	 */
	public function toArray()
	{
		if($this->isEmpty()) {
			return [
				'date_start'  => '',
				'time_start'  => '',
				'date_end'    => '',
				'time_end'    => '',
				'title'       => '',
				'url'         => '',
			];
		}

		return [
			'date_start'  => $this->dateStart,
			'time_start'  => $this->timeStart,
			'date_end'    => $this->dateEnd,
			'time_end'    => $this->timeEnd,
			'title'       => $this->title,
			'url'         => $this->url,
		];
	}
}
