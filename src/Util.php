<?php

namespace Rumedo\Banner;

class Util
{
	/**
	 * Переводит дату в формате "05.07.17" и время в формате "14:00"
	 * в timestamp в часовом поясе Europe/Moscow.
	 *
	 * Похоже, что Wordpress ставит часовой пояс UTC, так что глобально
	 * не стоит его изменять. При сравнении вместо now() следует использовать
	 * current_time('timestamp'). Настройки часового пояса хранятся в БД.
	 *
	 * @param $date
	 * @param $time
	 * @return int|bool
	 */
	public static function convertDateAndTimeStringsToTimestamp($date, $time)
	{
		$datetime = \DateTime::createFromFormat("d.m.y H:i", $date .' '. $time);
		if($datetime) {
			$datetime->setTimezone(new \DateTimeZone('Europe/Moscow'));
			return $timestamp = $datetime->getTimestamp();
		}
		return false;
	}
}
