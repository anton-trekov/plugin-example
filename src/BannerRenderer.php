<?php

namespace Rumedo\Banner;

/**
 * Class BannerRenderer
 *
 * Подключает шаблон
 *
 * @package Rumedo\Banner
 */
class BannerRenderer {
	/**
	 * Подключает файл шаблона
	 *
	 * @param $templateName string Название шаблона с расширением
	 * @param null|array $view Переменные шаблона
	 */
	static function render($templateName, $view=null)
	{
		$path = RUMEDO_BANNER_VIEWS_DIR .'/'. $templateName;
		if(file_exists($path)) {
			require($path);
		}
	}
}
