<?php

namespace Rumedo\Banner;

/**
 * Class CourseFetcher
 *
 * Получает курс с удаленного сайта. В случае если мероприятий нет или сайт упал,
 * проверка производится по интервалу, указанному в настройках
 *
 * @package Rumedo\Banner
 */
class CourseFetcher
{
	/**
	 * Получает данные о следующем мероприятии с другого сайта
	 *
     * @param $schoolKey string Идентификатор школы
	 * @return array
	 */
	public static function getData($schoolKey)
	{
		$data = [];

		if(self::canUpdate($schoolKey)) {
			$data = json_decode(file_get_contents(RUMEDO_BANNER_API_EXTERNAL_URI[$schoolKey]), true);
		}

		self::setLastUpdateTimeOption($schoolKey, $data);

		return $data;
	}

	/**
	 * Нельзя получать данные, если в настройках присутствует опция последнего времени обновления,
	 * но если с того времени прошел указанный в настройках интервал, то можно.
	 *
     * @param $schoolKey string Идентификатор школы
	 * @return bool
	 */
	private static function canUpdate($schoolKey)
	{
		$lastUpdateTime = get_option(RUMEDO_BANNER_WPOPTION_LASTUPDATES[$schoolKey]);
		if($lastUpdateTime) {
			if(current_time('timestamp') < $lastUpdateTime + RUMEDO_BANNER_UPDATE_INTERVAL) {
				return false;
			}
			return true;
		}
		return true;
	}

	/**
	 * Если с удаленного сайта не были получены данные, то установим опцию - время последнего обновления,
	 * чтобы получать данные по интервалу, указанному в настройках, а не при каждом запросе
	 *
     * @param $schoolKey string Идентификатор школы
	 * @param $data null|array
	 */
	private static function setLastUpdateTimeOption($schoolKey, $data)
	{
		if(! $data || (is_array($data) && 0 === count($data))) {
			update_option(RUMEDO_BANNER_WPOPTION_LASTUPDATES[$schoolKey], current_time('timestamp'));
		} else {
			delete_option(RUMEDO_BANNER_WPOPTION_LASTUPDATES[$schoolKey]);
		}
	}
}
