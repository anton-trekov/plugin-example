<?php

namespace Rumedo\Banner;

/**
 * Class Validator
 *
 * Производит валидацию актуальности курса.
 *
 * @package Rumedo\Banner
 */
class CourseValidator
{
	/**
	 * Проверяет актуальность курса на данный момент.
	 *
	 * Для сравнения временных меток используется current_time('timestamp'), а не now(),
	 * потому что в БД строки с временем и датой курса хранятся в часовом поясе Europe/Moscow
	 * я перевожу его в timestamp с явным указанием часового пояса. Похоже, что Wordpress
	 * устанавливает часовой пояс UTC и данные о часовом поясе хранит в БД, поэтому
	 * глобально его установить не получится.
	 *
	 * @param $course Course
	 * @return bool
	 */
	static function validateCourse(Course $course)
	{
		if($course->isEmpty()) {
			return false;
		}

		$courseTimestamp = Util::convertDateAndTimeStringsToTimestamp($course->getDateEnd(), $course->getTimeEnd());

		if($courseTimestamp && (current_time('timestamp') < $courseTimestamp)) {
			return true;
		}

		return false;
	}
}
