<?php

namespace Rumedo\Banner;

/**
 * Class Banner
 *
 * Класс для отображения баннера.
 *
 * Обладает состоянием, представленным в виде массива. Оно нужно для работы с
 * JS кодом. Если указано действие - 'need_update', js скрипт должен обновить
 * данные о мероприятии.
 *
 * @package Rumedo\Banner
 */
class Banner
{
	/** @var Course Курс в БД */
	private $course;

	/** @var string Идентификатор школы (используется как ключ в массивах с настройками) */
	private $schoolKey;


	/**
	 * Banner constructor.
     *
     * @param $schoolKey string Идентификтор школы, для которой получается баннер.
	 */
	public function __construct($schoolKey) {
	    $this->schoolKey = $schoolKey;
		$this->course = CourseManager::getCourse($schoolKey);
	}

	public function getSchoolKey()
    {
        return $this->schoolKey;
    }

	/**
	 * Возвращает массив с данными для отображении во view
	 *
	 * @return array
	 */
	public function getViewData()
	{
		return $this->course->toArray();
	}

	/**
	 * Не задан ли курс для баннера?
	 *
	 * Данная ситуация может возникнуть, если ближайших курсов нет.
	 *
	 * @return bool
	 */
	public function isEmpty()
	{
		if($this->course->isEmpty()) {
			return true;
		}
		return false;
	}
}
