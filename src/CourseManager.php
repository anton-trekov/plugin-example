<?php

namespace Rumedo\Banner;

/**
 * Class CourseManager
 *
 * Получает и сохраняет курс в БД.
 *
 * @package Rumedo\Banner
 */
class CourseManager
{
	/**
	 * Получает курс из БД.
	 *
     * @param $schoolKey string Идентификтор школы, для которой получается баннер
	 * @return Course
	 */
	static function getCourse($schoolKey)
	{
		$data = get_option(RUMEDO_BANNER_WPOPTIONS_FIELDS[$schoolKey]);
		$course = new Course($data);

		if( ! CourseValidator::validateCourse($course)) {
			$course = self::updateCourse($schoolKey);
		}

		return $course;
	}

	/**
	 * Получает данные о курсе по URL, сохраняет в БД и возвращает обновленный курс.
     *
     * @param $schoolKey string Идентификатор школы, для которой происходит обновление
     * @return Course
	 */
	static function updateCourse($schoolKey)
	{
		$data = CourseFetcher::getData($schoolKey);
		update_option(RUMEDO_BANNER_WPOPTIONS_FIELDS[$schoolKey], $data);
		return new Course($data);
	}
}
