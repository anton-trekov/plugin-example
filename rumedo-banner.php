<?php
/**
 * Plugin Name: Rumedo Banner
 *
 * Description: Показывает баннер с информацией о ближайшем вебинаре с одной из трех школ (случайно).
 *
 * Шаблон, который выводит данные о ближайшем вебинаре имеет в названии 'dynamic'.
 *
 * Получение данных сделано при помощи file_get_contents($url). Вариант с JS был недоделан
 * по причинам возможных проблем с безопасностью. Любой пользователь мог бы произвести
 * запись в БД, если бы узнал url и формат передаваемых данных.
 *
 */

if(!defined('ABSPATH')) exit;

//
// Константы
//

/** Путь до папки плагина */
define('RUMEDO_BANNER_DIR', plugin_dir_path(__FILE__));

/** Путь до папки views */
define('RUMEDO_BANNER_VIEWS_DIR', RUMEDO_BANNER_DIR .'/views/');

/** URL до папки /assets/ плагина */
define('RUMEDO_BANNER_URI', plugins_url(null, __FILE__) .'/');

/** uri директории с изображениями */
define('RUMEDO_BANNER_IMG_DIR_URI', RUMEDO_BANNER_URI .'/assets/img/');

//
// Autoloader, config
//
require_once(RUMEDO_BANNER_DIR .'/config.php');
require_once(RUMEDO_BANNER_DIR .'/vendor/autoload.php');

//
// Dynamic banner
//

// Получим случайный ключ школы из массива, который будем использовать в дальнейшем
$schoolKey = array_rand(RUMEDO_BANNER_WPOPTIONS_FIELDS);
$banner = new \Rumedo\Banner\Banner($schoolKey);

//
// Actions
//

// Баннер на главной странице
add_action('rumedo-banner-header', function() {
	Rumedo\Banner\BannerRenderer::render(RUMEDO_BANNER_SET['rumedo-banner-header']);
});

// Баннер на странице "обучение"
add_action('rumedo-banner-obuchenie', function() use ($banner) {
	if($banner->isEmpty()) {
		\Rumedo\Banner\BannerRenderer::render(RUMEDO_BANNER_SET['rumedo-banner-obuchenie']);
	} else {
		\Rumedo\Banner\BannerRenderer::render(RUMEDO_BANNER_SET['rumedo-banner-obuchenie-dynamic'][$banner->getSchoolKey()], $banner->getViewData());
	}
});
