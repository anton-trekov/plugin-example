<div class="container">
    <a class="dynamic-banner" href="<?= $view['url'] ?>?utm_source=abrumedo&utm_medium=bannerobuchenie" title="Следующее мероприятие школы гастроэнтерологии: <?= $view['title']; ?>" target="_blank">
        <span class="dynamic-banner__logo">
            <img src="<?= RUMEDO_BANNER_IMG_DIR_URI .'logo-gastro.jpg' ?>">
        </span>
        <span class="dynamic-banner__content">
            <span class="dynamic-banner__subscription">Следующее мероприятие:</span>
            <span class="dynamic-banner__text"><?= $view['title']; ?></span>
            <span class="dynamic-banner__datetime">
                <?php if($view['date_start'] === $view['date_end']) : ?>

                    <img class="dynamic-banner__icon" src="<?= RUMEDO_BANNER_IMG_DIR_URI .'icon.png' ?>">
                    <?= $view['date_start']; ?>
                    <img class="dynamic-banner__icon" src="<?= RUMEDO_BANNER_IMG_DIR_URI .'icon2.png' ?>">
                    <?= $view['time_start'] ?>-<?= $view['time_end']; ?>

                <?php else : ?>

                    <img class="dynamic-banner__icon" src="<?= RUMEDO_BANNER_IMG_DIR_URI .'icon.png' ?>">
                    <?= $view['date_start']; ?>
                    <img class="dynamic-banner__icon" src="<?= RUMEDO_BANNER_IMG_DIR_URI .'icon2.png' ?>">
	                <?= $view['time_start'] ?>
	                <span class="dynamic-banner__course-delimeter">&mdash;</span>
                    <img class="dynamic-banner__icon" src="<?= RUMEDO_BANNER_IMG_DIR_URI .'icon.png' ?>">
                    <?= $view['date_end']; ?>
                    <img class="dynamic-banner__icon" src="<?= RUMEDO_BANNER_IMG_DIR_URI .'icon2.png' ?>">
	                <?= $view['time_end']; ?>

                <?php endif; ?>
            </span>
        </span>
    </a>
</div>
