<?php
/*
 * Очищает информацию о мероприятиях из БД
 */

// @todo Разобраться - как же правильно все таки такие вещи определять
require_once(realpath('./../../../../wp-load.php'));
require_once(realpath('./../config.php'));

foreach(RUMEDO_BANNER_WPOPTIONS_FIELDS as $option) {
    delete_option($option);
}

foreach(RUMEDO_BANNER_WPOPTION_LASTUPDATES as $option) {
    delete_option($option);
}
