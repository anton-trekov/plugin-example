<?php

/** Название полей в wp_option для хранения данных о предстоящих вебинарах */
define("RUMEDO_BANNER_WPOPTIONS_FIELDS", [
    'pharma'    => 'rumedo_banner_data_pharma',
    'gastro'    => 'rumedo_banner_data_gastro',
    'neuro'     => 'rumedo_banner_data_neuro',
]);

/**
 * Название полей в wp_options для хранения времени последнего обновления,
 * если не было найдено следующего мероприятия на сайте-источнике.
 */
define('RUMEDO_BANNER_WPOPTION_LASTUPDATES', [
    'pharma'    => 'rumedo_banner_lastupdate_pharma',
    'gastro'    => 'rumedo_banner_lastupdate_gastro',
    'neuro'     => 'rumedo_banner_lastupdate_neuro',
]);

/** Адрес для получения данных о новом вебинаре */
define('RUMEDO_BANNER_API_EXTERNAL_URI', [
    'pharma'    => 'http://pharma.rumedo.ru/wp-content/plugins/rumedo-api/api/nearest-course.php',
    'gastro'    => 'http://gastro.rumedo.ru/wp-content/plugins/rumedo-api/api/nearest-course.php',
    'neuro'     => 'http://neuro.rumedo.ru/wp-content/plugins/rumedo-api/api/nearest-course.php',
]);

/** Интервал для получения информации о новых курсах в секунду, если новых мероприятий не обнаружено */
define('RUMEDO_BANNER_UPDATE_INTERVAL', 24*60*60*1);

/** Настройки action для вывода баннеров и файлов, которые будут подключаться */
define('RUMEDO_BANNER_SET', [
	'rumedo-banner-header'              => 'neuro-img-dark-banner.php',
	// Для динамического баннера нужно указать статическую заглушку на случай нехватки данных
	'rumedo-banner-obuchenie'           => 'neuro-img-light-banner.php',
	'rumedo-banner-obuchenie-dynamic'   => [
	    'pharma'    => 'pharma-dynamic-light-banner.php',
	    'gastro'    => 'gastro-dynamic-light-banner.php',
	    'neuro'     => 'neuro-dynamic-light-banner.php',
    ],
]);
